import React, { Component } from 'react';
import Board from './Board';
import Api from './Components/Data/getDataFromTrello';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
        boardData: '',
    }
}
componentDidMount() {
    Api.getData().then(resp => resp.json())
    .then(data => { 
        this.setState({
            boardData: data
        })
    })
}
  render(){
    const {name} = this.state.boardData;

    return(
      <div className= "App overflow-auto main-container">
        <div>
          <h1 style={{lineHeight:"100px", margin:"0px"}}>{name}</h1>
        </div>
        <Board/>
      </div>
    )
  }
}



export default App ;
