import React from "react";
import Api from '../Data/getDataFromTrello';
import { deleteCard } from '../Action/Action';

const style = {
    borderColor:"snow",
    borderRadius:"3px",
    width:"88%",
    resize:"none"
}

const cursorStyle = {
    cursor: "pointer"
}

class CardInput extends React.Component{
    constructor(props){
        super(props);
        this.state={
            cardName : "",
            id: ""
        }
    }

    getName = (e) => {
        this.setState({
            cardName: e.target.value,
            id: e.target.id
        })
    }

    passDatatoAddCard = (input) => {
        this.props.getInputValue(input, this.state.id)
        this.setState({
            cardName: "",
            id: ""
        })
    }

    deleteCard(id) {
        console.log(id, " delete id")
        Api.deleteCard(id);
        const { dispatch } = this.props;
        if(id){
        dispatch(deleteCard(id));
        }
        return;
    }
    
    render(){
    return (
        <div className="card-holder">
            <div className="card">
                <textarea id = {this.props.id } style = {style} onChange={this.getName} value={this.state.cardName} type="text" onSubmit={(e) => e.preventDefault()} placeholder="Add a new card here" />
            </div>
            <div className="card-content" style={{display:"flex"}}>
                <input type="submit" style = {cursorStyle} value="Add a Card" className="create-btn" onClick={() => this.passDatatoAddCard(this.state.cardName)} />
                <p className="close-card" style = {cursorStyle} onClick={() => this.props.onClickClose()} >X</p>
            </div>
        </div>);
    }
}

export default CardInput;