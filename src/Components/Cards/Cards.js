import React from 'react';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';
import Api from '../Data/getDataFromTrello';
import { deleteCard } from "../Action/Action";
import { connect } from 'react-redux';

class card extends React.Component {

    deleteCard(cardId, listId) {
        Api.deleteCardFromList(cardId)
        .then(response => response.json())
        .then(data => {
            console.log(data)
        })  
        .catch(console.log);
        const { dispatch } = this.props;
        if(cardId){
        dispatch(deleteCard(listId, cardId));
        }
        return;
    }
    render(){
        return(  
            <div className="cards">
                            {console.log(this.props.cards)}
            {this.props.cards.map((card, i) => {
            return(
            <div key = {i} className="card-container">
                <p id = {card.id}  >{ card.name } </p>
                <div id={this.props.id}>
                        <i className="fa fa-trash" style = {{"cursor": "pointer"}} onClick={this.deleteCard.bind(this, card.id, this.props.id)}></i>
                </div>
            </div>)})}
            </div>  
        )
    }
}

export default connect () (card);