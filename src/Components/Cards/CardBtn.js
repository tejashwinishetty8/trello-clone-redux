import React from "react";

const style = {
    cursor:"pointer", 
}

export default ({clickEvent}) => (
    <div>
        <div style = { style } className="card-btn-style" onClick={() => clickEvent()} >
            <p>+ Add another Card</p>
        </div>
    </div>
)