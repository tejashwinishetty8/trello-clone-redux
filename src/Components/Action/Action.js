export const getList = (lists) => {
    return{
        type: 'GET_LISTS',
        payload: lists
    };
}

export const addList = (list) => {
    console.log("Action ", list)
    return{
        type: 'ADD_LIST',
        payload: list
    };
};

export const addCard = (id, text) => {
    console.log("Action ", text)
    return{
        type: 'ADD_CARD',
        payload: { text, id }
    }
}

export const deleteList = (id) => {
    console.log(id, " action");
    return{
        type: 'DELETE_LIST',
        payload: id
    }
}

export const deleteCard = (ListId, cardId) => {
    console.log(cardId, " action");
    return{
        type: 'DELETE_CARD',
        payload: {ListId, cardId}
    }
}