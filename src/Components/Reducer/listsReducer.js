import uuid from 'uuid';

const initialstate = []

const listsReducer = (state = initialstate, action) => {
    console.log(state, "state");
    switch (action.type){
        case "GET_LISTS" :
        var lists = action.payload;
        return [...lists, ...state];

        case "ADD_LIST" :
        const newList = {
            name: action.payload,
            cards: [],
            id: uuid(),
        }
        return [...state, newList];

        case "DELETE_LIST" :
        const newDeletedList = state.filter((list) => list.id != action.payload)
        return newDeletedList;

        case "ADD_CARD" :
        const newCard = {
            id: uuid(),
            name: action.payload.text,
        };
        var arr = state.map(list => {
            if(list.id == action.payload.id){
                return {...list, cards: [...list.cards, newCard]}
            }else{
                return list;
            }
        });
        return arr;

        case "DELETE_CARD" :
        const newDeletedCardList = state.map((list) =>{
            if(list.id == action.payload.ListId){
                const cardList = list.cards.filter((card) => card.id != action.payload.cardId);
                return {...list, cards:[...cardList]}
            }
            else
            return list
        })
        return newDeletedCardList;
        default:
        return state;
    }
};

export default listsReducer;