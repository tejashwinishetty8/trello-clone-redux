let board = '5b235d1008f4396552a52c42'
let key = '6b3004cf200d26aba3c1e1982a6fa86a'
let token = 'b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2'

const getData = function(){
    return fetch(`https://api.trello.com/1/boards/${board}?key=${key}&token=${token}`);      
}

const getTrelloList = () => {
    return fetch(`https://api.trello.com/1/boards/${board}/lists?cards=open&checkItemStates=true&checklists=name&checklist_fields=all&sticker_fields=all&fields=all&key=${key}&token=${token}`);
}

const getCards = (id) => {
    return fetch(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`);
}

const createTrelloList = (name) => {
    return fetch(`https://api.trello.com/1/boards/${board}/lists?name=${name}&pos=bottom&key=${key}&token=${token}`, {
        method: 'POST'});
}
const createCard = (name, id) => {
    return fetch(`https://api.trello.com/1/cards?name=${name}&idList=${id}&keepFromSource=all&key=${key}&token=${token}`,
    { method: 'POST' });
}

const  deleteListFromTheBoard = (id) => {
    console.log("api enetered ", id)
    return fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`, { method: 'PUT' });
}

const  deleteCardFromList = (id) => {
    return fetch(`https://api.trello.com/1/cards/${id}?closed=true&key=${key}&token=${token}`, {
        method: 'PUT'});
}

const getCheckList = (cardId) => {
    return fetch(`https://api.trello.com/1/cards/${cardId}?attachment_fields=all&checklists=all&checklist_fields=all&sticker_fields=all&key=${key}&token=${token}`);
}

const getRequestForCheckListItem = (checklistid) => {
    return fetch(`https://api.trello.com/1/checklists/${checklistid}/checkItems?&key=${key}&token=${token}`);
}

const putrequestForCheckListItem = (cardId, id, checklistId, state) => {
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${id}?state=${state}&idChecklist=${checklistId}&key=${key}&token=${token}`, { method: 'PUT' });
}

const postRequestForCheckList = (cardId, nameOfCheckList) => {
    return fetch(`https://api.trello.com/1/checklists?idCard=${cardId}&name=${nameOfCheckList}&key=${key}&token=${token}`, {
        method: 'post'});
}

const postRequestForcheckListItem = (checklistid, newItem, checked) => {
    return fetch(`https://api.trello.com/1/checklists/${checklistid}/checkItems?name=${newItem}&pos=bottom&checked=${checked}&key=${key}&token=${token}`, { method: 'POST' })
}

export default {
    getData: getData,
    getTrelloList: getTrelloList,
    getCards: getCards,
    createTrelloList: createTrelloList,
    createCard: createCard,

    deleteListFromTheBoard: deleteListFromTheBoard,
    deleteCardFromList:  deleteCardFromList,

    getCheckList: getCheckList,
    getRequestForCheckListItem: getRequestForCheckListItem,
    
    putrequestForCheckListItem: putrequestForCheckListItem,
    postRequestForCheckList: postRequestForCheckList,
    postRequestForcheckListItem: postRequestForcheckListItem
}