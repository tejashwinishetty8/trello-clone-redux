import React from "react";

const style = {
    cursor:"pointer", 
    backgroundColor:"rgb(0,0,0,.24)"
}

export default ({clickEvent}) => (
    <div>
        <div style = { style } className="btn-style" onClick={() => clickEvent()} >
            <span>+</span> Add another List
        </div>
    </div>
)