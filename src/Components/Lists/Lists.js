import React from 'react';
import Card from '../Cards/Cards';
import CardInput from '../Cards/CardInput';
import CardInputBtn from '../Cards/CardBtn';
import { connect } from 'react-redux';
import Api from '../Data/getDataFromTrello';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';
import { deleteList } from "../Action/Action";



class TrelloList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showInput: false,          
        }
    }
    handleClose = () => {
        this.setState({
            showInput: false
        })
    }

    handleClick = () => {
        this.setState({
            showInput: true
        })
    }
    getInputValue = (inputName, id) => {
        this.props.getCardData(inputName, id)
        
    }
    deleteList(id) {
        console.log(id, " delete id")
        Api.deleteListFromTheBoard(id)
        .then(response => response.json())
        .then(data => {
            console.log(data)
        })  
        .catch(console.log);
        const { dispatch } = this.props;
        if(id){
        dispatch(deleteList(id));
        }
        return;
    }
    render(){
        
    return(
        <div className="lists-container">
            <div className="container">
                <div className="lists">
                    <h3 className="list-name" id={this.props.id}>{this.props.name}</h3>
                    <div>
                        <i className="fa fa-trash" style = {{"cursor": "pointer"}} onClick={this.deleteList.bind(this, this.props.id)}></i>
                    </div>
                </div>
                    <Card id = {this.props.id} cards = { this.props.cards }></Card>
                <div className="input-button" >
                    {this.state.showInput ? <CardInput id = {this.props.id} onClickClose={this.handleClose}  getInputValue={this.getInputValue} /> : <CardInputBtn clickEvent={this.handleClick}/>}       
                </div>
            </div>
        </div>
    ); 
}
}

export default connect () (TrelloList) ;
