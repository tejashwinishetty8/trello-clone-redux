import React from "react";
import TextArea from 'react-textarea-autosize';

const style = {
    cursor :"pointer",
    resize : "none",
    width : "300",
    outline : "none",
    height: "100%"
}

class CardInput extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listName : ""
        }
    }

    getName = (e) => {
        console.log(e.target.value);
        this.setState({
            listName: e.target.value
        })
    }

    passDatatoAddList = (input) => {
        this.props.getInputValue(input)
        this.setState({
            listName: ""
        })
    }
    
    render(){
    return (<div className="create-list">
            <form className="createList" style = {style} onSubmit={(e) => e.preventDefault()}>
                <TextArea style = {style} onChange={this.getName} value={this.state.listName} autoFocus type="text" /> 
            </form>
            <div className="extra-content">
            <input type="submit" style = {{cursor : "pointer"}} value="Add List" className="create-btn" onClick={() => this.passDatatoAddList(this.state.listName)}/>
            <p className="close" style = {{cursor : "pointer"}} onClick={() => this.props.onClickClose()}>X</p>
            </div>
    </div>);
    }
}

export default CardInput;