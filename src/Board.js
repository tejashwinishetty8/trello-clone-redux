import React, { Component } from 'react';
import TrelloList from './Components/Lists/Lists';
import { connect } from 'react-redux';
import ListInput from './Components/Lists/ListInput';
import ListBtn from './Components/Lists/ListBtn';
import { addList, addCard, getList } from "./Components/Action/Action";
import Api from './Components/Data/getDataFromTrello';

class Board extends Component {

    constructor(props){
      super(props);
      this.state = {
          showInput: false,          
        }
    }

    handleClose = () => {
        this.setState({
            showInput: false
        })
    }
    
    handleClick = () => {
        this.setState({
            showInput: true
        })
    }

    getInputValue = (inputName) => {
        Api.createTrelloList(inputName)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                })  
                .catch(console.log);
                
        const { dispatch } = this.props;
        if(inputName){
            dispatch(addList(inputName));
        }
        return;
    }

    getCardData = (text, listId) => {
        Api.createCard(text, listId)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                })  
                .catch(console.log);
    
        const { dispatch } = this.props;
        if(text){
        dispatch(addCard(listId,text));
        }
        return;
    }

    componentDidMount() {
        Api.getTrelloList().then(resp => resp.json())
            .then(data => {
            console.log(data, "data")
            const { dispatch } = this.props;
            if(data){
                dispatch(getList(data));
            }
            return;      
            });
    }

    render(){

        const { lists } = this.props;
        console.log(lists, " lists");

        return(
            <div>
                <div className = "main-list-container" style = {{display: "flex", justifyContent:"start", margin:"10px"}}>
                        {lists.map((list, i) => <TrelloList getCardData = { this.getCardData} key = {i} id= {list.id} name = {list.name} cards = {list.cards}/>)}
                    <div className="input-button" >
                        {this.state.showInput ? <ListInput onClickClose={this.handleClose}  getInputValue={this.getInputValue} /> : <ListBtn clickEvent={this.handleClick}/>}       
                    </div>
                </div>
            </div>
        ) 
    }
}
  
  const mapStateToProps = state => ({
    lists: state.lists
  })
  
  export default connect (mapStateToProps) (Board) ;
  